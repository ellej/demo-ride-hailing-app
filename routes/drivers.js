const express = require('express');
const router = express.Router();
const Driver = require('../models/driver');

router.get('/', (req, res, next) => {
  Driver
    .aggregate([
      {
        $geoNear: {
          near: {
            type: 'Point',
            coordinates: [
              parseFloat(req.query.lng),  // longitude first
              parseFloat(req.query.lat)   // latitude second
            ]
          },
          distanceField: 'dist.calculated',
          maxDistance: 10000,
          query: {
            isAvailable: true,
            'car.numSeats': { $gte: parseInt(req.query.numSeats) }
          },
          spherical: true
        }
      },
      { $limit: 10 }
    ])
    .then(nearbyDrivers => res.send(nearbyDrivers))
    .catch(err => next(err));
});

router.post('/', (req, res, next) => {
  Driver
    .create(req.body)
    .then(driver => res.status(201).send(driver))
    .catch(err => next(err));
});

router.put('/:id', (req, res, next) => {
  Driver
    .findByIdAndUpdate(req.params.id, req.body, { new: true })
    .then(driver => {
      if (!driver)
        return next({
          status: 404,
          message: 'There is no driver with the given ID.'
        });

      res.send(driver);
    })
    .catch(err => next(err));
});

router.delete('/:id', (req, res, next) => {
  Driver
    .findOneAndDelete({_id: req.params.id})
    .then(driver => {
      if (!driver)
        return next({
          status: 404,
          message: 'There is no driver with the given ID.'
        });
  
      res.send(driver);
    })
    .catch(err => next(err));
});


// using async and await

/*
router.get('/', async (req, res, next) => {
  try {
    const nearbyDrivers = await Driver.aggregate([
      {
        $geoNear: {
          near: {
            type: 'Point',
            coordinates: [
              parseFloat(req.query.lng),
              parseFloat(req.query.lat)
            ]
          },
          distanceField: 'dist.calculated',
          maxDistance: 10000,
          query: {
            isAvailable: true,
            'car.numSeats': { $gte: parseInt(req.query.numSeats) }
          },
          spherical: true
        }
      },
      { $limit: 10 }
    ]);

    res.send(nearbyDrivers)
  }
  catch (err) {
    next(err);
  }
});

router.post('/', async (req, res, next) => {
  try {  
    const driver = await Driver.create(req.body);
    res.status(201).send(driver);
  }
  catch (err) {
    next(err);
  }
});

router.put('/:id', async (req, res, next) => {
  try {  
    const driver = await Driver.findByIdAndUpdate(
      req.params.id, req.body, { new: true }
    );
    if (!driver)
      return next({
        status: 404,
        message: 'There is no driver with the given ID.'
      });

    res.send(driver);
  }
  catch (err) {
    next(err);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {  
    const driver = await Driver.findOneAndDelete({ _id: req.params.id });
    if (!driver)
      return next({
        status: 404,
        message: 'There is no driver with the given ID.'
      });

    res.send(driver);
  }
  catch (err) {
    next(err);
  }
});
*/

module.exports = router;
