const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// a GeoJSON object consists of a "type" and "coordinates" field
const geoJsonSchema = new Schema({
  type: {
    type: String,
    default: 'Point'
  },
  // since we will only be dealing with Point types,
  // the coordinates should be an array of numbers
  // (other types may need nested arrays)
  coordinates: {
    type: [Number],
    required: true
  }
});

const driverSchema = new Schema({
  // the field with the GeoJSON object
  // (this field needs to be top-level, not nested)
  location: geoJsonSchema,

  // other fields
  name: {
    type: String,
    minlength: 5,
    maxlength: 50,
    required: true
  },
  phone: {
    type: String,
    minlength: 5,
    maxlength: 50,
    required: true
  },
  isAvailable: {
    type: Boolean,
    default: false
  },
  car: {
    numSeats: {
      type: Number,
      min: 2,
      max: 10,
      required: true
    }
  }
});

// create a 2dsphere index on the field holding the GeoJSON data
driverSchema.index({ location: '2dsphere' });

const Driver = mongoose.model('Driver', driverSchema);

module.exports = Driver;
