# Demo - Ride-Hailing Application (backend)

Illustrates how to write geospatial queries to MongoDB by setting up a ride-hailing application.

Complementary blog post: [https://www.ellej.dev/blog/how-to-write-geospatial-queries-to-mongodb-by-setting-up-a-ride-hailing-app](https://www.ellej.dev/blog/how-to-write-geospatial-queries-to-mongodb-by-setting-up-a-ride-hailing-app)

## Tech Stack

  * Node.js
  * MongoDB
  * Express

## How to Run the Application

1. This implementation assumes you have:
    * Node installed
    * MongoDB installed locally on your computer (see next section for download instructions). If you'd rather use MongoDB Atlas (cloud) you may replace the database in app.js.
2. Clone the repo
3. Open a terminal window and navigate to the project's root directory
4. Run `npm install`
5. Run `node app.js` 

## How to Run MongoDB Locally

1. Download MongoDB Compass (local database)
    * Go to: [https://www.mongodb.com/download-center/compass](https://www.mongodb.com/download-center/compass)
    * Select the latest version of: *Community Edition Stable*
    * Select the platform you are on
    * Click "Download"
2. Install MongoDB Compass and get it started
    * Go to: [https://docs.mongodb.com/manual/administration/install-community/](https://docs.mongodb.com/manual/administration/install-community/)
    * Select the guide appropriate for your operating system and follow the steps
