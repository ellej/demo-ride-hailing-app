const mongoose = require('mongoose');
const express = require('express');
const driversRouter = require('./routes/drivers');

const app = express();

// select and connect to the database
// (this will automatically create a local database called
// "my_ride_hailing_app" if one does not already exist)
const db = process.env.NODE_ENV === 'test'
  ? 'mongodb://localhost/my_ride_hailing_app_test'
  : 'mongodb://localhost/my_ride_hailing_app';

// make sure to set "useCreateIndex" to true
mongoose
  .connect(db, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true
  })
  .then(() => console.log(`Connected to ${db}...`))
  .catch((err) => {
    console.error(`Error connecting to ${db}...`);
    process.exit(0);
  });

// parse incoming JSON objects
app.use(express.json());

// have the driversRouter handle all requests to /api/drivers
app.use('/api/drivers', driversRouter);

// handle requests to invalid endpoints
app.use((req, res, next) => {
  const err = new Error(`Route could not be found: ${req.url}`);
  err.status = 404;
  next(err);
});

// handle errors from the request processing pipeline
app.use((err, req, res, next) => {
  res
    .status(err.status || 500)
    .send({ error: err.message || 'Something went wrong...' });
});

// listen for requests
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Server running on port ${port}...`));
